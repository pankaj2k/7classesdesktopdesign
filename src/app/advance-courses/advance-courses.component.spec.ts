import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceCoursesComponent } from './advance-courses.component';

describe('AdvanceCoursesComponent', () => {
  let component: AdvanceCoursesComponent;
  let fixture: ComponentFixture<AdvanceCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvanceCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
